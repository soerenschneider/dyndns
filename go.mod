module dyndns

go 1.14

require (
	github.com/aws/aws-sdk-go v1.38.39
	github.com/eclipse/paho.mqtt.golang v1.3.4
	github.com/hashicorp/go-retryablehttp v0.7.0
	github.com/prometheus/client_golang v1.10.0
)
