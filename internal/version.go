package internal

var (
	BuildVersion string
	BuildTime    string
	CommitHash   string
)
